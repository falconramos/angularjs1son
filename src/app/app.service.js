angular
    .module('app')
    .service('ContactService',function(){
        var list=[];
        this.getList = function(){
            return list;
        }
        this.setlist = function(_list){
            list = _list;
        }

        this.push = function(contact){
            list.push(contact);
        }
    });


// angular
//     .module('app')
//     .service('MathService',function(MathFactory){
//         /*
//             no caso de definir variáveis dentro do serviço pode ser dessa forma

//             var data = {};

//             this.getData = function(){
//                 return data;
//             }

//             this.setData = function(_data){
//                 data = _data;
//             }
//         */

//         this.mulService = MathFactory.mul;
//         this.divService = MathFactory.div;

//         /*
//             Ou pode ser feito assim

//             this.mulService = function(num1,num2){
//                 return MathFactory.mul(num1,num2);
//             }
//             this.divService = funcion(num1,num2){
//                 return MathFactory.div(num1,num2);
//             }
//         */
//     });