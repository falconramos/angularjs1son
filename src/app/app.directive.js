angular
    .module('app')
    .directive('appDirective', function(){
        return {
            // restrict: 
            template: "<h1>My Directive</h1>",
            scope: {
                name: '@',
                lastname: '@'
            },
            link: function($scope, $element, $attr){
                console.log($scope)
            }
            // bindController:
            // required:
            
        }
    })
    .directive('maskTel', function(){
        return {
            link: function($scope, $element, $attr, ctrl){
                $element.bind('keyup',function(){
                    var _formatTel = function(value){
                        value = value.replace(/[^0-9]/g,"");
                        
                        if(value.length > 2 && value.length <=6){
                            value = "(" + value.substring(0,2) + ")" + value.substring(2,6);
                        }
                        else if(value.length > 6 && value.length <= 10){
                            value = "(" + value.substring(0,2) + ")" + value.substring(2,6) + "-" + value.substring(6,10);
                        }else if(value.length > 10){
                            value = "(" + value.substring(0,2) + ")" + value.substring(2,7) + "-" + value.substring(7,11);
                        }

                        return value;
                    }
                    $element[0].value = _formatTel($element[0].value);
                    //console.log($element[0].value);
                });
            }            
        }
    });