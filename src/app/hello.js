angular
  .module('app')
  .component('app', {
    templateUrl: 'app/hello.html',
    controller: function(ContactFactory,$filter) {
      var vm = this;
      var id = 0;

      vm.date = new Date();

      vm.form = {
        id: 0,
        name: '',
        phone: '',
        email: ''
      };

      vm.listContact = [];

      vm.add = add;
      vm.editContact = editContact;
      vm.delContact = delContact;
      vm.orderedBy = orderedBy;
      vm.view = view;

      vm.title = $filter("uppercase")("Contatos");
      vm.formtitle = "Novo Contato";
      vm.listTitle = "Lista de Contatos";

      //momento que iniciar
      (function onInit(){
        return vm.listContact = ContactFactory.list();
      })();

      function add(contact){
        if(!contact){
          alert('Necessário ter um contato válido');
          return;
        }

        if(contact.id){
          return ContactFactory.edit(contact);
          cleanForm();
        }

        contact.id = id = id+1;
        
        ContactFactory.add(contact);

        cleanForm();
      }

      function view(){
        return ContactFactory.view();
      }

      function editContact(contact, indexList){
        if(!contact){
          alert('Necessário ter um contato válido');
          return;
        }

        vm.form.name = contact.name;
        vm.form.phone = contact.phone;
        vm.form.email = contact.email;
        vm.form.id = contact.id;
        
      }

      function delContact(contact){
        if(!contact){
          alert('Necessário ter um contato válido');
          return;
        }

        ContactFactory.remove(contact);
      }

      function orderedBy(field){
        vm.orderResult = field;
        vm.orderWay = !vm.orderWay;
      }

      function cleanForm(){
        vm.form = {
          id: 0,
          name: '',
          phone: '',
          email: ''
        };
      }

    }

    // controller: function (MathFactory,MathService) {
    //   var vm = this;
    //   vm.hello = 'Hello World teste!';
    //   vm.person = {
    //     name: '',
    //     lastname: ''
    //   };

    //   vm.listPerson = [{
    //     name: 'Joao',
    //     lastname: 'Souza'
    //   },{
    //     name: 'Luiz',
    //     lastname: 'Cesar'
    //   },{
    //     name: 'Carlos',
    //     lastname: 'Eduardo'
    //   }];

    //   vm.alertName = function(){
    //     alert(vm.person.name+' '+vm.person.lastname);
    //   };

    //   vm.sum = function(num1,num2){
    //     return alert(MathFactory.sum(num1,num2));
    //   };

    //   vm.sub = function(num1,num2){
    //     return alert(MathFactory.sub(num1,num2));
    //   };

    //   vm.mul = function(num1,num2){
    //     return alert(MathService.mulService(num1,num2));
    //   };

    //   vm.div = function(num1,num2){
    //     return alert(MathService.divService(num1,num2));
    //   };
    // }
  });
